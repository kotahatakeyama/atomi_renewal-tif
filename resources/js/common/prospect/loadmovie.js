//OPENING MOVIE
$(function(){
	var visits = $.cookie('visits') || 0;
	visits++;

	$.cookie('visits', visits, { path: '/jh/' });

	if ( $.cookie('visits') > 1 ) {
		$('#active-popup').hide();
		$('#site-cover').hide();
	} else {
			var pageHeight = $(document).height();
			$('<div id="active-popup"></div>').insertBefore('body');
			$('#active-popup').css("height", pageHeight);
			$('#site-cover').show();
      		setTimeout(function(){
  			$("#active-popup,#site-cover").stop().fadeOut("slow");
   			// $("#active-popup,#site-cover").stop().animate({opacity:'0'},1000);
    		},150000);
	}

	if ($.cookie('noShowWelcome')) { $('#site-cover').hide(); $('#active-popup').hide(); }
});

$(document).mouseup(function(e){
	var container = $('#site-cover','.site-cover-btn-skip');

	if( !container.is(e.target)&& container.has(e.target).length === 0)
	{
		container.fadeOut();
		$('#active-popup').fadeOut();
		$('#site-cover').fadeOut();
	}

});



