$(function(){
//SP背景交互
    $('.panels-item:even').addClass('even');
    $('.ghd-access-list').hide();
});

//SPボタン　バス
$(function(){
	$('.ghd-access-list').hide();
	  $(".ghd-btn-access a").on("click", function() {
	  $('.ghd-access-list').slideToggle();
	  $(this).toggleClass("open");
  });
});

// スムーススクロール
$(function(){
  $('a[href^=#]').click(function() {
    var speed = 400; // ミリ秒
    var href= $(this).attr("href");
    var target = $(href == "#" || href == "" ? 'html' : href);
    var position = target.offset().top;

    $('body,html').animate({scrollTop:position}, speed, 'swing');
    return false;
  });
});

///INSTAGRAM
$(document).ready(function($){
	setCommon();
});
function setCommon(){
	//insta gallery
	var instaArray = new Array();
	var $gallerylist = $("#js-gallerylist");
	$gallerylist.text("loading...");
	getInstagram();

	function getInstagram() {
		$.ajax({
			type: "GET",
			url: "https://api.instagram.com/v1/users/self/media/recent",
			data: { access_token: "3955294295.1229104.9727a877780c4d4ba7b686da525896e8" },
			dataType: "jsonp",
			error: function(jqXHR, textStatus, errorThrown) {
				$gallerylist.text(textStatus);
			},
			success: function(data, textStatus, jqXHR) {
				$gallerylist.text("");
				$gallerylist.append("<ul class='instagram-list'></ul></ul>");
				for (var i = 0, length = 16; i < length; i++) {
					var d = data.data[i];
					$gallerylist.children('ul').append(
						$("<li>").append(
							$("<a>").attr("href", d.link).attr("target", "_blank").append(
								$("<img>").attr("src", d.images.thumbnail.url)
								)
							)
						);
				}
			}
		});
	}
}


