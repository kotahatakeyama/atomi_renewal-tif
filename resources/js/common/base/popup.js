// popup(modal)

(function ( window ){
  $(document).ready(function($){

    //  Menu Modal
    $('.js-popup-modal').magnificPopup({
      type: 'inline',
      preloader: false,
      fixedContentPos: true,
      mainClass: 'mfp-fade'
    });

    //閉じるリンクの設定
    $(document).on('click', '.js-popup-modal-close', function (e) {
      e.preventDefault();
      $.magnificPopup.close();
    });

  });
}( window ));

