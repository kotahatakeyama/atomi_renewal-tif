$(function () {
  var headerHight = 64; //ヘッダーの高さ
  var speed = 400; //スムースのミリ秒
  $('a[href^=#]').click(function(){
    var href= $(this).attr("href");
    var target = $(href == "#" || href == "" ? 'html' : href);
    var positionSP = target.offset().top-headerHight;
    var positionPC = target.offset().top;
    if($('.ghd-menu-btn').css('display') === 'block' && !($(this).hasClass('js-popup-modal'))){
      $("html, body").animate({scrollTop:positionSP}, speed, "swing");
    } else {
      $("html, body").animate({scrollTop:positionPC}, speed, "swing");
    }
    return false;
  });
});
