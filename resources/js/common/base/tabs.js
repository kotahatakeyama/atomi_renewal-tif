// tab

$(function() {
  $('.tab-menu li').on('click', function() {
    var index = $('.tab-menu li').index(this);
    $('.tab-contents > .tab-contents-container').css('display','none');
    $('.tab-contents > .tab-contents-container').eq(index).css('display','block');
    $('.tab-menu li').removeClass('js-show');
    $(this).addClass('js-show')
  });
});
