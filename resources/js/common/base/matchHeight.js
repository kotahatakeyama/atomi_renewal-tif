// ブロック要素の高さ揃え

(function ( window ){
  $(document).ready(function($){
    $('.nav-global-box-item').matchHeight();
    $('.link-box-item a').matchHeight();
    $('.heading-child-box').matchHeight();
    $('.article-list-title').matchHeight();
    $('.article-list-tags').matchHeight();
    $('.tab-menu li').matchHeight();
    $('.prospect .ghd-nav-page a').matchHeight();
    $('.prospect .box-relation-link .relation-link-item').matchHeight();
    $('.entranceguide-result-site .box-emphasize').matchHeight();
    $('.uniform-image-box .box-emphasize').matchHeight();
    $('.badge-song-box').matchHeight();
  });
}( window ));
