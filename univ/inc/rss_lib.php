<?php

class RssParser {
	
	public $error = false;
	public $items = null;
	
	function __construct ($path, $tag='') {
		$this->items = array();
		$xml = simplexml_load_file($path);
		if ($xml) {
			if ($tag) {
				foreach ($xml->channel->item as $item) {
					$tags = explode(',', $item->tag);
					if (in_array($tag, $tags)) {
						array_push($this->items, $item);
					}
				}
			}
			else {
				foreach ($xml->channel->item as $item) {
					array_push($this->items, $item);
				}
			}
		}
		else {
			$this->error = true;
		}
	}
	
	public function filter ($icon) {
		$items = array();
		foreach ($this->items as $item) {
			$icons = explode(',', $item->icon);
			if (in_array($icon, $icons)) {
				array_push($items, $item);
			}
		}
		return $items;
	}
}

?>